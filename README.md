# BadBSOD

A DigiSpark BadUSB script that causes an almost instantaneous Blue Screen of Death (BSOD) on Windows 10 computers. 

Among finding myself in a dark place, I started thinking of ways I could sodomize Windows machines using a BadUSB to lighten my mood. Unfortunately, all of the simple scripts that I looked at either used patched methods of causing strain, crashing, or damage to Windows machines, or required administrative privileges. Finally I came across [this](https://github.com/peewpw/Invoke-BSOD/blob/master/Invoke-BSOD.ps1) and saw that I could invoke a BSOD almost instantly with a single command to download and execute a PowerShell script. The beautiful thing about this is that it doesn't require administrative privileges, nor will it most likely be patched as it's an undocumented feature as opposed to an exploit.

## How to use it
1. Get a hold of a DigiSpark ATTINY85. This device gets registered as an HID (human interface device), thus allowing us to send keyboard sequences through the USB device.
2. Install Arduino and follow [this tutorial](http://digistump.com/wiki/digispark/tutorials/connecting) to configure Arduino for your ATTINY85.
3. Upload the code to the board
4. Plug the BadBSOD-equipped device into a Windows 10 PC, and wait while it does it work to crash the system.